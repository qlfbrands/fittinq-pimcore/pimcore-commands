<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\Exception;

use UnexpectedValueException;

class NotAConcreteObjectException extends UnexpectedValueException
{

}