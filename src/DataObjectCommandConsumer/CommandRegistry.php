<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\DataObjectCommandConsumer;


use Fittinq\Pimcore\Commands\Exception\CommandNotFoundException;

class CommandRegistry
{
    /**
     * @var CommandHandler[]
     */
    private array $commands = [];

    public function addCommand(string $commandType, CommandHandler $commandHandler): void
    {
        $this->commands[$commandType] = $commandHandler;
    }

    public function getCommand(string $commandType): CommandHandler
    {
        if (!key_exists($commandType, $this->commands)) {
            throw new CommandNotFoundException("Could not find command: " . $commandType);
        }

        return $this->commands[$commandType];
    }
}