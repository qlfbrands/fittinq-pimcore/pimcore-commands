<?php declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\Mock;

use Fittinq\Pimcore\Commands\DataObjectCommandConsumer\CommandHandler;
use PHPUnit\Framework\Assert;

class HandlerMock extends CommandHandler
{
    private int $executedId;

    public function execute(int $objectId): void
    {
        $this->executedId = $objectId;
    }

    public function assertHandlerCalledWithObjectId(int $objectId): void
    {
        Assert::assertEquals($objectId, $this->executedId);
    }
}