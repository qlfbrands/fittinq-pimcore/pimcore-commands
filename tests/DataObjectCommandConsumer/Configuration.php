<?php declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\DataObjectCommandConsumer;

use Exception;
use Fittinq\Pimcore\Commands\DataObjectCommandConsumer\CommandHandler;
use Fittinq\Pimcore\Commands\DataObjectCommandConsumer\CommandRegistry;
use Fittinq\Pimcore\Commands\DataObjectCommandConsumer\DataObjectCommandConsumer;
use Fittinq\Symfony\RabbitMQ\ErrorLogging\NullErrorLogger;
use stdClass;
use Test\Fittinq\Pimcore\Commands\Mock\HandlerMock;
use Test\Fittinq\Pimcore\Commands\Mock\LockFactoryMock;

class Configuration
{
    private DataObjectCommandConsumer $dataObjectEventListener;
    private CommandRegistry $commandRegistry;
    private HandlerMock $handlerMock;
    private LockFactoryMock $lockFactory;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->commandRegistry = new CommandRegistry();
        $this->dataObjectEventListener = new DataObjectCommandConsumer('queue', new NullErrorLogger(), $this->commandRegistry);
        $this->lockFactory = new LockFactoryMock();
        $this->handlerMock = new HandlerMock($this->lockFactory);
    }

    public function configure(): DataObjectCommandConsumer
    {
        return $this->dataObjectEventListener;
    }

    public function getHandlerMock(): HandlerMock
    {
        return $this->handlerMock;
    }

    public function getLockFactory(): LockFactoryMock
    {
        return $this->lockFactory;
    }


    public function createMessage(int $objectId, string $commandType): stdClass
    {
        $message = new stdClass();
        $message->objectId = $objectId;
        $message->commandType = $commandType;
        return $message;
    }

    public function registerCommand(string $commandType, CommandHandler $handler): void
    {
        $this->commandRegistry->addCommand($commandType, $handler);
    }
}