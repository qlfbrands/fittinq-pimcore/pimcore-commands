<?php
declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\Mock;

use PHPUnit\Framework\Assert;
use Symfony\Component\Lock\Key;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;

class LockFactoryMock extends LockFactory
{
    private array $locks = [];

    public function __construct()
    {
        parent::__construct(new StoreMock());
    }

    public function createLock(string $resource, ?float $ttl = 300.0, bool $autoRelease = true): LockInterface
    {
        $lock = new LockMock(new Key($resource));
        $this->locks[] = $lock;
        return $lock;
    }

    public function getLock(int $index): LockMock
    {
        return $this->locks[$index];
    }

    public function assertCount(int $count): void
    {
        Assert::assertCount($count, $this->locks);
    }
}