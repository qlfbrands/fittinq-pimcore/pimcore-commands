<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PimcoreCommandsBundle extends Bundle
{
}
